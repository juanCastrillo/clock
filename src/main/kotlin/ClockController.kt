import javax.swing.*
import java.awt.*
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent

class ClockController(relojDig: Boolean, relojAng: Boolean) : JFrame() {

    var marc: DigitalClockFrame? = null
    var marc2: AnalogClockFrame? = null
    private val pos: Point
    private val hora: ShortArray
    private var parado = false
    private var wasDoubleClick: Boolean = false

    internal var lastEvent: MouseEvent? = null
    internal var timer: Timer = Timer(0, null)

    init {
        hora = currentHora()
        if (relojDig) {
            setSize(500, 100)
            relojDig()
        } else if (relojAng) {
            setSize(500, 500)
            relojAnalog()
        }

        val dim = Toolkit.getDefaultToolkit().screenSize
        pos = Point(dim.width / 2 - this.size.width / 2, dim.height / 2 - this.size.height / 2)

        this.setLocation(pos.x, pos.y)
        isUndecorated = true
        background = Color(0, 0, 0, 0)
        this.isVisible = true
        this.addMouseListener(object : MouseAdapter() {
            override fun mousePressed(e: MouseEvent?) {
                //System.out.print("click");
                pos.x = e!!.x
                pos.y = e.y
            }

            override fun mouseClicked(e: MouseEvent?) {
                if (e!!.clickCount == 2) {
                    if (relojAng) {
                        marc2!!.stop()
                        marc2 = null
                        relojAnalog()
                    }
                    if (relojDig) {
                        marc!!.stop()
                        marc = null
                        relojDig()
                    }
                    wasDoubleClick = true
                } else {

                    val timerinterval = Toolkit.getDefaultToolkit().getDesktopProperty("awt.multiClickInterval") as Int
                    timer = Timer(timerinterval) {
                        if (wasDoubleClick) {
                            wasDoubleClick = false // reset flag
                        } else {
                            if (!parado) {
                                parado = true
                                if (relojAng) marc2!!.stop()
                                if (relojDig) marc!!.stop()
                            } else {
                                parado = false
                                if (relojAng) marc2!!.continuation()
                                if (relojDig) marc!!.continuation()
                            }
                        }
                    }

                    timer.isRepeats = false

                    timer.start()
                }
            }


        })

        this.addMouseMotionListener(object : MouseAdapter() {
            override fun mouseDragged(evt: MouseEvent?) {
                //sets frame position when mouse dragged
                setLocation(evt!!.xOnScreen - pos.x, evt.yOnScreen - pos.y)

            }
        })


    }

    private fun currentHora(): ShortArray {
        /*Date hola = new java.util.Date();
        Format formatter = new SimpleDateFormat("HH.mm.ss");
        String x = formatter.format(hola);
        String[] hi = x.split(Pattern.quote("."));*/
        return shortArrayOf(0, 0, 0)
    }

    private fun relojDig() {
        marc = DigitalClockFrame(this.size, hora)
        marc!!.isVisible = true
        add(marc)
    }

    private fun relojAnalog() {
        marc2 = AnalogClockFrame(this.size, hora)
        marc2!!.isVisible = true
        add(marc2)
    }

    companion object {
        private val clickInterval = Toolkit.getDefaultToolkit().getDesktopProperty("awt.multiClickInterval") as Int
    }
}
