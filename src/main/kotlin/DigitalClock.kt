import javax.swing.*
import java.awt.*

class DigitalClock internal constructor(hora: Short, minutos: Short, segundos: Short, private val posicion: Point, private val size: Dimension)
    : Clock(24.toShort(), hora, minutos, segundos) {
    private val center: Int = 0

    fun dibujarReloj(g: Graphics) {
        g.color = Color.white
        g.fillRect(0, 0, size.width * 2, size.height)

        val g2 = g as Graphics2D
        g2.stroke = BasicStroke(10f)
        g.setColor(Color(10, 150, 255))
        g2.drawRect(0, 0, size.width * 2, size.height)
    }
}
