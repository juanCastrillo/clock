import java.awt.*
import java.util.Timer

class AnalogClock internal constructor(hour: Short, minutes: Short, seconds: Short, private val posicion: Point, private val size: Int)
    : Clock(12.toShort(), hour, minutes, seconds) {
    private val center: Int = 0

    fun avanzar() {
        clockHands.second++
        if (clockHands.second == nSeconds) {
            clockHands.second = 0
            clockHands.minute++
            if (clockHands.minute == nMinutes) {
                clockHands.minute = 0
                clockHands.hour++
                if (clockHands.hour == nHours) {
                    clockHands.hour = 0
                }
            }
        }
    }

    fun actualizarReloj(g: Graphics) {
        //avanzar();
        clockOutline(g)
        drawClock(g)
        clockDetails(g)
    }

    override fun drawClock(g: Graphics) {

        val g2 = g as Graphics2D
        g2.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON)

        //manecilla seconds
        g2.stroke = BasicStroke(1f)
        g.setColor(Color.red)
        g2.drawLine(posicion.x, posicion.y, mseconds().x, mseconds().y)

        //manecilla hours
        g2.stroke = BasicStroke(2f)
        g2.color = Color.black
        g2.drawLine(posicion.x, posicion.y, mhours().x, mhours().y)

        //manecilla minutes
        g2.stroke = BasicStroke(2f)
        g.setColor(Color(10, 150, 255))
        g2.drawLine(posicion.x, posicion.y, mminutes().x, mminutes().y)
    }

    private fun clockOutline(g: Graphics) {
        val g2 = g as Graphics2D
        g2.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON)

        //relleno del reloj
        g.setColor(Color.white)
        g2.fillArc(posicion.x - 3 * size / 4, posicion.y - 3 * size / 4, 3 * size / 2, 3 * size / 2, 0, 360)

        //marco del reloj
        g.setColor(Color(10, 150, 255))
        g2.stroke = BasicStroke(15f)
        g2.drawArc(posicion.x - 3 * size / 4, posicion.y - 3 * size / 4, 3 * size / 2, 3 * size / 2, 0, 360)

    }

    private fun clockDetails(g: Graphics) {
        g.color = Color.black
        g.fillArc(posicion.x - 4, posicion.y - 4, 8, 8, 0, 360)

        for (i in 0 until nHours) {
            val bordehour = Point(posicion.x, posicion.y)
            val ratio = 2 * Math.PI / nHours
            val longitud = (size * 0.05).toInt()
            val anguloActual = ratio * i - Math.PI / 2
            bordehour.x += (Math.cos(anguloActual) * size.toDouble() * 0.71).toInt()
            bordehour.y += (Math.sin(anguloActual) * size.toDouble() * 0.71).toInt()
            val finalP = Point(bordehour.x, bordehour.y)
            finalP.x -= (Math.cos(anguloActual) * longitud).toInt()
            finalP.y -= (Math.sin(anguloActual) * longitud).toInt()

            g.drawLine(bordehour.x, bordehour.y, finalP.x, finalP.y)
        }
    }

    private fun mhours(): Point {
        val mhour = Point(posicion.x, posicion.y)
        val ratio = 2 * Math.PI / nHours
        val longitud = (size * 0.5).toInt()
        val anguloActual = ratio * clockHands.hour - Math.PI / 2
        mhour.x += (Math.cos(anguloActual) * longitud).toInt()
        mhour.y += (Math.sin(anguloActual) * longitud).toInt()
        return mhour
    }

    private fun mminutes(): Point {
        val mminute = Point(posicion.x, posicion.y)
        val ratio = 2 * Math.PI / nMinutes
        val longitud = (size * 0.7).toInt()
        val anguloActual = ratio * clockHands.minute - Math.PI / 2
        mminute.x += (Math.cos(anguloActual) * longitud).toInt()
        mminute.y += (Math.sin(anguloActual) * longitud).toInt()
        return mminute
    }

    private fun mseconds(): Point {
        val msecond = Point(posicion.x, posicion.y)
        val ratio = 2 * Math.PI / nSeconds
        val longitud = (size * 0.7).toInt()
        val anguloActual = ratio * clockHands.second - Math.PI / 2
        msecond.x += (Math.cos(anguloActual) * longitud).toInt()
        msecond.y += (Math.sin(anguloActual) * longitud).toInt()
        return msecond
    }
}
