import javax.swing.*
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener

class DigitalClockFrame internal constructor(size: Dimension, hour: ShortArray) : JPanel(), ActionListener {

    private val clock: DigitalClock
    private val timer = Timer(1000, this)
    private val marcadores: Array<JLabel>

    init {
        marcadores = initiateMarcadores(6)
        layout = FlowLayout()

        clock = DigitalClock(hour[0], hour[1], hour[2], Point(size.width / 2, size.height / 2), Dimension(size.width / 2, size.height))
        background = Color(0, 0, 0, 0)
        timer.start()
    }

    private fun initiateMarcadores(indexes:Int):Array<JLabel> {
        val numbers = Array<JLabel>(indexes) { JLabel()}
        numbers.forEach { it.font = Font("Consolas", Font.ITALIC, 100) }
        return numbers
    }

    private fun setMarcadores() {
        var hour = ""
        var minute = ""
        var second = ""
        if (clock.clockHands.hour < 10) hour = "0"
        marcadores[0].text = hour + clock.clockHands.hour
        marcadores[1].text = ":"

        if (clock.clockHands.minute < 10) minute = "0"
        marcadores[2].text = minute + clock.clockHands.minute
        marcadores[3].text = ":"

        if (clock.clockHands.second < 10) second = "0"
        marcadores[4].text = second + clock.clockHands.second
    }

    fun stop() {
        try {
            timer.stop()
            //clock.wait()
        } catch (x: Exception) {
        }

    }

    fun continuation() {
        timer.start()
        //clock.notify()
    }

    override fun actionPerformed(e: ActionEvent) {
        clock.work()
        setMarcadores()
    }

    public override fun paintComponent(g: Graphics) {
        clock.drawClock(g)
    }
}
