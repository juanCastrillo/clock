import java.awt.*
import java.util.Date

abstract class Clock internal constructor(nHours: Short, hour: Short, minutes: Short, seconds: Short) {

    protected var nHours:Short = 24
    protected val nMinutes:Short = 60
    protected val nSeconds:Short = 60

    protected val hours: ShortArray
    protected val minutes: ShortArray
    protected val seconds: ShortArray

    var clockHands: ClockHands

    protected var funcionar = Thread(Runnable { this.work() })

    init {
        var hour = hour
        var minutes = minutes
        var seconds = seconds

        this.nHours = nHours
        this.hours = initialize(nHours)
        this.minutes = initialize(nMinutes)
        this.seconds = initialize(nSeconds)

        //inicializamos hours
        if (hour !in 0..nHours)
            hour = 0

        //inicializamos minutes
        if (minutes !in 0..nMinutes)
            minutes = 0

        //inicializamos seconds
        if (seconds !in 0..nSeconds)
            seconds = 0

        clockHands = ClockHands(hour, minutes, seconds)


    }

    fun work() {

        while (clockHands.hour < nHours) {
            while (clockHands.minute < nMinutes) {
                while (clockHands.second < nSeconds) {
                    clockHands.second++
                    try {
                        Thread.sleep(1000)
                    } catch (e: Exception) {
                    }

                    //System.out.println(clockHands.toString());
                }
                clockHands.second = 0
                clockHands.minute++
            }
            clockHands.minute = 0
            clockHands.hour++
        }
    }

    /*public void funcionar() {

        if(clockHands.hour < nHours){
            if(clockHands.minute < nMinutes){
                if (clockHands.second < nSeconds){
                    clockHands.second ++;
                }
                clockHands.second = 0;
                clockHands.minute ++;
            }
            clockHands.minute =0;
            clockHands.hour ++;
        }
    }*/

    open fun drawClock(g: Graphics) {

    }

    fun parar() {
        try {
            funcionar.stop()
            print("parar plox")
        } catch (x: Exception) {
            println("no puedo parar: $x")
        }

    }

    fun continuar() {
        try {
            funcionar.start()
        } catch (x: Exception) {
        }

    }

    private fun maxValue(shorts: ShortArray): Short {
        var max: Short = 0
        for (i in shorts.indices) {
            if (shorts[i] > max) {
                max = shorts[i]
            }
        }
        return max
    }

    private fun initialize(size: Short): ShortArray {
        val unidades = ShortArray(size.toInt())
        for (i in unidades.indices) {
            unidades[i] = i.toShort()
        }
        return unidades
    }

    override fun toString(): String {
        return clockHands.toString()
    }
}
