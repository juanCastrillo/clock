import javax.swing.*
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener

class AnalogClockFrame
//private Thread avanzar = new Thread(this::avanzar);

internal constructor(size: Dimension, hora: ShortArray) : JPanel(), ActionListener {

    var relo: AnalogClock

    private val timer = Timer(1000, this)

    init {

        background = Color(0, 0, 0, 0)
        setSize(size.width, size.height)
        relo = AnalogClock(hora[0], hora[1], hora[2], Point(size.width / 2, size.height / 2), size.width / 2)
        timer.start()

    }

    public override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
        relo.actualizarReloj(g)
    }

    fun stop() {
        try { timer.stop() } catch (x: Exception) { }

    }

    fun continuation() { timer.start() }

    override fun actionPerformed(e: ActionEvent) {
        repaint()
        relo.avanzar()
    }

    fun avanzar() {
        for (i in 0..59) {
            relo.avanzar()
            repaint()
            try { Thread.sleep(15) } catch (c: Exception) {}

        }
    }
}
